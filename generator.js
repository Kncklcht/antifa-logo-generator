const download = () => {                       
  img = new Image(),
    serializer = new XMLSerializer(),
    svgStr = serializer.serializeToString(document.getElementById('svg'));
  img.src = 'data:image/svg+xml;base64,'+window.btoa(svgStr);
  img.onload = () => {
    const canvas = document.createElement("canvas");
    const w=2048;
    const h=2048;
    canvas.width = w;
    canvas.height = h;
    canvas.getContext("2d").drawImage(img,0,0,w,h);
    const imgURL = canvas.toDataURL("image/png");
    const dlLink = document.createElement('a');
    dlLink.download = "image";
    dlLink.href = imgURL;
    dlLink.dataset.downloadurl = ["image/png", dlLink.download, dlLink.href].join(':');
    document.body.appendChild(dlLink);
    dlLink.click();
    ocument.body.removeChild(dlLink);
  }
}

const getValue = (id, defaultValue) => {
  const value = document.getElementById(id).value;
  return value === '' ? defaultValue : value;
}

const createGs = (svgns) => {
  const outerG = document.createElementNS(svgns, 'g');
  const innerG = document.createElementNS(svgns, 'g');
  outerG.setAttribute('transform', 'matrix(1.25,0,0,-1.25,0,367.35)');
  innerG.setAttribute('transform', 'scale(0.12,0.12)');
  return [outerG, innerG];
}

const createLogoBase = (svgns, backgroundColor, circleColor, frontFlagColor, backFlagColor) => {
  const background = document.createElementNS(svgns, 'circle');
  const circle = document.createElementNS(svgns, 'path');
  const frontFlag = document.createElementNS(svgns, 'path');
  const backFlag = document.createElementNS(svgns, 'path');
  background.setAttribute('cx', '184');
  background.setAttribute('cy', '183');
  background.setAttribute('r', '175');
  background.setAttribute('fill', backgroundColor);
  circle.setAttribute('id', 'circle');
  circle.setAttribute('d', 'M 1226.9688,33 C 568.61487,33 32,569.60726 32,1227.9688 c 0,658.3641 536.61441,1195 1194.9688,1195 658.3639,0 1195,-536.6361 1195,-1195 C 2421.9688,569.60741 1885.3322,33 1226.9688,33 z m 0,271.65625 c 511.5565,0 923.3437,411.75984 923.3437,923.31255 0,511.556 -411.7877,923.3437 -923.3437,923.3437 -511.54371,0 -923.31255,-411.7879 -923.31255,-923.3437 0,-511.55256 411.76838,-923.31255 923.31255,-923.31255 z');
  circle.setAttribute('fill', circleColor);
  frontFlag.setAttribute('d', 'm 1594.84,412.418 437.46,1210.662 -35.41,20.83 c -106.25,95.83 -152.09,156.25 -264.59,200 -173.05,67.3 -316.66,-64.58 -464.58,-104.17 -227.28,-60.82 -436.49,-44.92 -628.157,82.16 -20.594,13.66 -27.668,27.07 -43.75,45.84 -135.418,-150.01 -240.032,-343.1 -260.223,-566.33 -0.59,-6.52 1.019,-10.2 2.082,-16.66 45.836,-37.5 375.914,-347.332 836.328,-182.75 186.21,66.56 310.42,-8.33 470.84,-108.332 26.1,-16.273 24.5,-16.016 39.58,-27.082 l -197.92,-600 52.09,14.582 56.25,31.25');
  frontFlag.setAttribute('fill', frontFlagColor);
  backFlag.setAttribute('d', 'M 1451.09,1054.09 1190.05,333.039 c -12.89,-0.531 -20.22,-0.621 -33.06,0.555 -22.9,2.101 -35.92,4.949 -57.64,12.5 l 132.99,393.406 -20.84,14.586 c -77.08,29.164 -111.89,53.996 -214.578,35.414 -218.75,-39.582 -504.766,62.648 -611.742,286.32 -22.914,47.92 -35.653,69.93 -43.34,118.55 -3.164,20.02 -4.184,31.54 -4.168,51.81 210.375,-174.17 423.109,-262.332 713.418,-212.93 136.41,23.21 223.37,94.55 352.08,43.75 19.27,-7.61 29.96,-12.59 47.92,-22.91');
  backFlag.setAttribute('fill', backFlagColor);
  return [background, circle, frontFlag, backFlag];
}

const createLogoText = (svgns, textColor, textTop, textBottom) => {
  const topPath = document.createElementNS(svgns, 'path');
  const topText = document.createElementNS(svgns, 'text');
  const bottomPath = document.createElementNS(svgns, 'path');
  const bottomText = document.createElementNS(svgns, 'text');
  topPath.setAttribute('id', 'curve2');
  topPath.setAttribute('fill', 'transparent');
  topPath.setAttribute('d', `
    M 184,183
    m 0,146
    a 146,146 0 1,1 0,-292
    a 146,146 0 1,1 0,292
  `);
  topText.setAttribute('width', '500');
  topText.setAttribute('text-anchor', 'middle');
  topText.setAttribute('font-family', 'Helvetica');
  topText.setAttribute('font-weight', '1000');
  topText.innerHTML = '<textPath alignment-baseline="top" startOffset="50%" xlink:href="#curve2" style="font-size:35px" fill="' + textColor + '">' + textTop.toUpperCase() + '</textPath>';
  bottomPath.setAttribute('id', 'curve');
  bottomPath.setAttribute('fill', 'transparent');
  bottomPath.setAttribute('d', `
    M 184,183
    m 0,-171
    a 171,171 0 1,0 0,342
    a 171,171 0 1,0 0,-342
  `);
  bottomText.setAttribute('width', '500');
  bottomText.setAttribute('text-anchor', 'middle');
  bottomText.setAttribute('font-family', 'Helvetica');
  bottomText.setAttribute('font-weight', '1000');
  bottomText.innerHTML = '<textPath alignment-baseline="top" startOffset="50%" xlink:href="#curve" style="font-size:35px" fill="' + textColor + '">' + textBottom.toUpperCase() + '</textPath>';
  return [topPath, topText, bottomPath, bottomText];
}

const render = () => {
  const textTop = getValue('textTop', 'antifaschistische');
  const textBottom = getValue('textBottom', 'aktion');
  const textColor = getValue('textColor', 'white');
  const backgroundColor = getValue('backgroundColor', 'transparent');
  const circleColor = getValue('circleColor');
  const frontFlagColor = getValue('frontFlagColor');
  const backFlagColor = getValue('backFlagColor');
  const svg = document.getElementById('svg');
  const svgns = 'http://www.w3.org/2000/svg';

  const [outerG, innerG] = createGs(svgns);
  const [background, circle, frontFlag, backFlag] = createLogoBase(svgns, backgroundColor, circleColor, frontFlagColor, backFlagColor);
  const [topPath, topText, bottomPath, bottomText] = createLogoText(svgns, textColor, textTop, textBottom);

  svg.innerHTML = '';
  innerG.appendChild(backFlag);
  innerG.appendChild(frontFlag);
  innerG.appendChild(circle);
  outerG.appendChild(innerG);
  svg.appendChild(background);
  svg.appendChild(outerG);
  svg.appendChild(topPath);
  svg.appendChild(topText);
  svg.appendChild(bottomPath);
  svg.appendChild(bottomText);
}

render();
